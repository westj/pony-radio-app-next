# Pony Radio App Next

## A multiple station web app built with NextJS and React.

*Designed to use a common interface, with obvious stylistic branding from each of the radio stations included.*

### Some mockups:

![alt text](https://i.imgur.com/xPPSP1q.png "PonyvilleFM Desktop Mock")
![alt text](https://i.imgur.com/F8lQeiB.png "Fillydelphia Radio Desktop Mock")
![alt text](https://i.imgur.com/1Hu11cr.png "PonyvilleFM Mobile Proof of Concept")
![alt text](https://i.imgur.com/RkoZ6gu.png "Fillydelphia Radio Mobile Proof of Concept")

